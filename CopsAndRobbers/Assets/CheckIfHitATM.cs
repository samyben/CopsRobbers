﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CheckIfHitATM : MonoBehaviour
{
    private GameObject player;

    void Start()
    {
        player = GameObject.FindGameObjectsWithTag("Player").Where(p => p.GetComponent<PlayerInfo>().usableWeapon == gameObject).FirstOrDefault();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ATM")
        {
            AtmInfos atmInfo = other.GetComponent<AtmInfos>();

            if (atmInfo.isWorking)
            {
                int motorIndex = 0; // the first motor
                float motorLevel = 1.0f; // full motor speed
                float duration = 0.3f; // 2 seconds
                ReInput.players.GetPlayer(player.GetComponent<PlayerInfo>().playerRewiredID).SetVibration(motorIndex, motorLevel, duration);

                atmInfo.ActivateAlarm(player);
            }
        }
    }
}
