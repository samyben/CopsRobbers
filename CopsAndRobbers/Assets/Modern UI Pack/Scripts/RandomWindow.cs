﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomWindow : MonoBehaviour {
    public Material WindowOn;
    public Material WindowOff;

    // Use this for initialization
    void Start () {
        int random = Random.Range(1, 3);

        if (random >= 2)
            GetComponent<MeshRenderer>().material = WindowOff;
        else
            GetComponent<MeshRenderer>().material = WindowOn;
    }
}
