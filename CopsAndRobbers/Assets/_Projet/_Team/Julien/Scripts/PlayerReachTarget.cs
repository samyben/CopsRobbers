﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerReachTarget : MonoBehaviour {

    private PlayerInfo playerInfo;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerInfo>().isSavingGold = true; ;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            other.GetComponent<PlayerInfo>().isSavingGold = false; ;
    }
}