﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerPC;
    public Text timerVR;
    public float totalTime = 120f; //2 minutes
    public float criticalSecondBeforeEnd = 30;
    public Color baseColor;
    public Color criticalColor;

    private static Timer instance;
    private bool isCriticalTime = false;
    private EventSystem _eventSystem;
    private bool isMenuLoaded = false;

    private Timer() { }

    public static Timer Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new Timer();
            }
            return instance;
        }
    }

    private void Start()
    {
        timerPC.color = baseColor;
        timerVR.color = baseColor;

        Time.timeScale = 1.0f;
        _eventSystem = EventSystem.current;
    }

    private void Update()
    {
        totalTime -= Time.deltaTime;
        UpdateLevelTimer(totalTime);
    }

    public void UpdateLevelTimer(float totalSeconds)
    {

        if (totalSeconds > 0)
        {
            if (!isCriticalTime && totalSeconds <= criticalSecondBeforeEnd)
            {
                isCriticalTime = true;
                timerPC.color = criticalColor;
                timerVR.color = criticalColor;
            }
            int minutes = Mathf.FloorToInt(totalSeconds / 60f);
            int seconds = Mathf.RoundToInt(totalSeconds % 60f);

            string formatedSeconds = seconds.ToString();

            if (seconds == 60)
            {
                seconds = 0;
                minutes += 1;
            }
            if (timerPC != null)
            {
                timerPC.text = minutes.ToString("00") + ":" + seconds.ToString("00");
            }
            if (timerVR != null)
                timerVR.text = minutes.ToString("00") + ":" + seconds.ToString("00");
        }
        else
        {
            if (!isMenuLoaded)
            {
                isMenuLoaded = true;
                EventManager.TimeOver();
            }
        }
    }
}
