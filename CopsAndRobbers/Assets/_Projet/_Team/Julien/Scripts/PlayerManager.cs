﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance = null;

    private PlayerState state;
    public GameObject[] players;
    public GameObject[] objectsToEnable;
    public GameObject[] objectsToDisable;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    enum PlayerState
    {
        IDLE,
        WALK,
        RUN,
        INVISIBLE,
        VISIBLE,
        GRABBED,
        JAILED,
        ESCAPED,
        TIMEOVER,
    }

    public void OnEnable()
    {
        EventManager.OnIdle += Idle;
        EventManager.OnWalk += Walk;
        EventManager.OnRun += Run;
        EventManager.OnInvisible += Invisible;
        EventManager.OnVisible += Visible;
        EventManager.OnGrabbed += Grabbed;
        EventManager.OnJailed += Jailed;
        EventManager.OnEscaped += Escaped;
        EventManager.OnTimeOver += TimeOver;
    }


    public void OnDisable()
    {
        EventManager.OnIdle -= Idle;
        EventManager.OnWalk -= Walk;
        EventManager.OnRun -= Run;
        EventManager.OnInvisible -= Invisible;
        EventManager.OnGrabbed -= Grabbed;
        EventManager.OnJailed -= Jailed;
        EventManager.OnEscaped -= Escaped;
        EventManager.OnTimeOver -= TimeOver;
    }

    public void Idle()
    {
        state = PlayerState.IDLE;
    }

    public void Walk()
    {
        state = PlayerState.WALK;
    }

    public void Run(GameObject player, float speedMultiplier)
    {
        state = PlayerState.RUN;
        player.GetComponent<Move>().moveSpeed = player.GetComponent<Move>().moveSpeedAtStart * speedMultiplier;
    }

    public void Invisible(GameObject player)
    {
        state = PlayerState.INVISIBLE;
        ChangePlayerVisibility(player, false);

    }

    public void Visible(GameObject player)
    {
        state = PlayerState.VISIBLE;
        ChangePlayerVisibility(player, true);
    }

    public void Grabbed(GameObject player)
    {
        state = PlayerState.GRABBED;

        PlayerInfo pi = player.GetComponent<PlayerInfo>();
        if (pi.goldOnPlayer > 0)
        {
            Debug.Log("Lost Gold");
            if (!pi.goldCoin.isPlaying)
            {
                pi.goldCoin.Play();
            }
            pi.goldOnPlayer = pi.goldOnPlayer - UnityEngine.Random.Range(1, 4);
            pi.goldAmount.text = pi.goldOnPlayer.ToString();
        }
        else
        {
            Debug.Log("No More Gold");
            if (pi.goldCoin.isPlaying)
            {
                pi.goldCoin.Stop();
            }
            pi.goldOnPlayer = 0;
            pi.goldAmount.text = "0";
        }
    }

    public void Jailed(GameObject player, Transform[] jailStart)
    {
        state = PlayerState.JAILED;
        PlayerInfo pi = player.GetComponent<PlayerInfo>();
        pi.gameObject.layer = LayerMask.NameToLayer("UngrabbablePlayer");
        pi.isJailed = true;
        pi.jailedCount++;
        pi.goldOnPlayer = 0;
        pi.goldAmount.text = "0";
        int random = UnityEngine.Random.Range(0, jailStart.Length);
        player.transform.position = jailStart[random].position;
        player.transform.rotation = jailStart[random].rotation;
    }

    public void Escaped(GameObject player)
    {
        state = PlayerState.ESCAPED;
        PlayerInfo playerInfo = player.GetComponent<PlayerInfo>();
        playerInfo.goldSecured += playerInfo.goldOnPlayer;
        playerInfo.goldOnPlayer = 0;
        playerInfo.goldAmount.text = playerInfo.goldOnPlayer.ToString();
    }

    private void TimeOver()
    {
        state = PlayerState.TIMEOVER;

        foreach (var player in players)
        {
            player.GetComponent<rotateCamera>().enabled = false;
        }
    }

    void ChangePlayerVisibility(GameObject player, bool isVisible)
    {
        PlayerInfo pi = player.GetComponent<PlayerInfo>();
        PostProcessingProfile ppProfile = pi.playerCamera.GetComponent<PostProcessingBehaviour>().profile;
        VignetteModel.Settings vignetteSettings = ppProfile.vignette.settings;

        if (pi != null)
        {
            if (!isVisible)
            {
                player.layer = LayerMask.NameToLayer("UngrabbablePlayer");
                pi.body.GetComponent<SkinnedMeshRenderer>().material = pi.invisibleMat;
                pi.beard.GetComponent<MeshRenderer>().material = pi.invisibleMat;
                pi.axe.GetComponent<MeshRenderer>().material = pi.invisibleMat;
                pi.hair.GetComponent<MeshRenderer>().material = pi.invisibleMat;
                pi.bag.GetComponent<SkinnedMeshRenderer>().material = pi.invisibleMat;

                vignetteSettings.intensity = 0.55f;
                ppProfile.vignette.settings = vignetteSettings;
            }
            else
            {
                player.layer = LayerMask.NameToLayer("Player");
                pi.body.GetComponent<SkinnedMeshRenderer>().material = pi.bodyMat;
                pi.beard.GetComponent<MeshRenderer>().material = pi.beardMat;
                pi.axe.GetComponent<MeshRenderer>().material = pi.axeMat;
                pi.hair.GetComponent<MeshRenderer>().material = pi.hairMat;
                pi.bag.GetComponent<SkinnedMeshRenderer>().material = pi.bagMat;

                vignetteSettings.intensity = 0f;
                ppProfile.vignette.settings = vignetteSettings;
            }


        }
    }


}
