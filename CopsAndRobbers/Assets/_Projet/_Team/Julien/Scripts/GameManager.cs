﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    private GameState state;
    private AsyncOperation asyncLoadLevel;

    public GameObject loadingCanvas;

    public GameObject[] startPosition;

    public List<GameObject> players;
    [HideInInspector]
    public int numberOfPlayer;
    public Text goldAmountTextP1;
    public Text goldAmountTextP2;
    public Text goldAmountTextP3;
    public Text goldAmountTextP4;

    public Text nameP1;
    public Text nameP2;
    public Text nameP3;
    public Text nameP4;

    public Image powerUpImgP1;
    public Image powerUpImgP2;
    public Image powerUpImgP3;
    public Image powerUpImgP4;

    public Image goldAmountImgP1;
    public Image goldAmountImgP2;
    public Image goldAmountImgP3;
    public Image goldAmountImgP4;

    public GameObject ResultP1;
    public GameObject ResultP2;
    public GameObject ResultP3;
    public GameObject ResultP4;

    public Text winnerText;

    enum GameState
    {
        GAME_START,
        PLAYER_REACH_TARGET,
        TIME_OVER,
        GAME_OVER,
        SHOW_SCORE,
        SHOW_MENU,
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        if (GameObject.FindGameObjectWithTag("Player") != null)
        {
            Debug.Log(GameObject.FindGameObjectWithTag("Player"));
            DontDestroyOnLoad(GameObject.FindGameObjectWithTag("Player"));
        }


    }

    public void OnEnable()
    {
        EventManager.OnGameStart += GameStart;
        EventManager.OnTimeOver += TimeOver;
        EventManager.OnPlayerReachTarget += PlayerReachTarget;
        EventManager.OnGameOver += GameOver;
        EventManager.OnShowScore += ShowScore;
        EventManager.OnShowMenu += ShowMenu;
    }

    public void OnDisable()
    {
        EventManager.OnGameStart -= GameStart;
        EventManager.OnTimeOver -= TimeOver;
        EventManager.OnPlayerReachTarget -= PlayerReachTarget;
        EventManager.OnGameOver -= GameOver;
        EventManager.OnShowScore -= ShowScore;
        EventManager.OnShowMenu -= ShowMenu;
    }

    private void GameStart(string playScene, int playerNumber)
    {
        state = GameState.GAME_START;
        StartCoroutine(LoadGameScene(playScene, playerNumber));

        Time.timeScale = 1;
    }

    private void GameOver()
    {
        state = GameState.GAME_OVER;
        StartCoroutine(LoadEndScene("EndMenu", players, true));
        Time.timeScale = 0;
    }

    private void ShowScore()
    {
        state = GameState.SHOW_SCORE;
    }

    private void ShowMenu()
    {
        state = GameState.SHOW_MENU;
    }

    private void PlayerReachTarget()
    {
        state = GameState.PLAYER_REACH_TARGET;
    }

    private void TimeOver()
    {
        state = GameState.TIME_OVER;

        StartCoroutine(LoadEndScene("EndMenu", players, false));
        Time.timeScale = 0;
    }

    IEnumerator LoadGameScene(string playScene, int playerNumber)
    {
        numberOfPlayer = playerNumber;
        asyncLoadLevel = SceneManager.LoadSceneAsync(playScene, LoadSceneMode.Single);

        while (!asyncLoadLevel.isDone)
        {
            print("Loading the Scene ... " + asyncLoadLevel.progress);
            yield return null;
        }
        players = GameObject.FindGameObjectsWithTag("Player").ToList();

        FindPlayersUI(playerNumber);
        DisablePlayer(playerNumber);
        SetPlayersCam(playerNumber);
        SetPlayersStartPosition();
    }

    private void SetPlayersStartPosition()
    {
        startPosition = GameObject.FindGameObjectsWithTag("StartPosition");
        foreach (GameObject player in players)
        {
            int random = UnityEngine.Random.Range(0, startPosition.Length);
            player.transform.position = startPosition[random].transform.position;
            player.transform.rotation = startPosition[random].transform.rotation;
        }
    }

    private void FindPlayersUI(int playerNumber)
    {
        goldAmountTextP1 = GameObject.FindGameObjectWithTag("GoldP1").GetComponent<Text>();
        goldAmountTextP2 = GameObject.FindGameObjectWithTag("GoldP2").GetComponent<Text>();
        goldAmountTextP3 = GameObject.FindGameObjectWithTag("GoldP3").GetComponent<Text>();
        goldAmountTextP4 = GameObject.FindGameObjectWithTag("GoldP4").GetComponent<Text>();

        powerUpImgP1 = GameObject.FindGameObjectWithTag("PowerUpP1").GetComponent<Image>();
        powerUpImgP2 = GameObject.FindGameObjectWithTag("PowerUpP2").GetComponent<Image>();
        powerUpImgP3 = GameObject.FindGameObjectWithTag("PowerUpP3").GetComponent<Image>();
        powerUpImgP4 = GameObject.FindGameObjectWithTag("PowerUpP4").GetComponent<Image>();

        goldAmountImgP1 = GameObject.FindGameObjectWithTag("GoldAmountImgP1").GetComponent<Image>();
        goldAmountImgP2 = GameObject.FindGameObjectWithTag("GoldAmountImgP2").GetComponent<Image>();
        goldAmountImgP3 = GameObject.FindGameObjectWithTag("GoldAmountImgP3").GetComponent<Image>();
        goldAmountImgP4 = GameObject.FindGameObjectWithTag("GoldAmountImgP4").GetComponent<Image>();
    }

    private void DisablePlayer(int playerNumber)
    {
        List<GameObject> playerToRemove = new List<GameObject>();
        for (int i = playerNumber; i < players.Count; i++)
        {
            players[i].SetActive(false);
            playerToRemove.Add(players[i]);
        }
        foreach (GameObject player in playerToRemove)
        {
            players.Remove(player);
        }
    }

    private void SetPlayersCam(int playerNumber)
    {
        PlayerInfo pi;
        switch (playerNumber)
        {
            case 1:
                Debug.Log("1P");
                goldAmountTextP2.transform.parent.gameObject.SetActive(false);
                goldAmountTextP3.transform.parent.gameObject.SetActive(false);
                goldAmountTextP4.transform.parent.gameObject.SetActive(false);

                powerUpImgP2.transform.parent.gameObject.SetActive(false);
                powerUpImgP3.transform.parent.gameObject.SetActive(false);
                powerUpImgP4.transform.parent.gameObject.SetActive(false);

                pi = players[0].GetComponent<PlayerInfo>();
                pi.playerCamera.rect = new Rect(0, 0, 1, 1);
                pi.goldAmount = goldAmountTextP1;
                pi.goldAmountImg = goldAmountImgP1;
                pi.powerupImg = powerUpImgP1;
                pi.playerRewiredID = 0;
                break;
            case 2:
                Debug.Log("2P");
                goldAmountTextP3.transform.parent.gameObject.SetActive(false);
                goldAmountTextP4.transform.parent.gameObject.SetActive(false);

                powerUpImgP3.transform.parent.gameObject.SetActive(false);
                powerUpImgP4.transform.parent.gameObject.SetActive(false);
                for (int i = 0; i < playerNumber; i++)
                {
                    Debug.Log(i + " " + players[i]);
                    switch (i)
                    {
                        case 0:
                            pi = players[i].GetComponent<PlayerInfo>();
                            pi.playerRewiredID = 0;
                            pi.goldAmount = goldAmountTextP1;
                            pi.goldAmountImg = goldAmountImgP1;
                            pi.powerupImg = powerUpImgP1;
                            pi.playerCamera.rect = new Rect(-0.5f, 0, 1, 1);
                            break;
                        case 1:
                            pi = players[i].GetComponent<PlayerInfo>();
                            pi.playerRewiredID = 1;
                            pi.goldAmount = goldAmountTextP2;
                            pi.goldAmountImg = goldAmountImgP2;
                            pi.powerupImg = powerUpImgP2;
                            pi.playerCamera.rect = new Rect(0.5f, 0, 1, 1);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 3:
                goldAmountTextP4.transform.parent.gameObject.SetActive(false);

                powerUpImgP4.transform.parent.gameObject.SetActive(false);
                Debug.Log("3P");
                for (int i = 0; i < playerNumber; i++)
                {
                    switch (i)
                    {
                        case 0:
                            pi = players[i].GetComponent<PlayerInfo>();
                            pi.playerRewiredID = 0;
                            pi.goldAmount = goldAmountTextP1;
                            pi.goldAmountImg = goldAmountImgP1;
                            pi.powerupImg = powerUpImgP1;
                            pi.playerCamera.rect = new Rect(-0.5f, 0, 1, 1);
                            break;
                        case 1:
                            pi = players[i].GetComponent<PlayerInfo>();
                            pi.playerRewiredID = 1;
                            pi.goldAmount = goldAmountTextP2;
                            pi.goldAmountImg = goldAmountImgP2;
                            pi.powerupImg = powerUpImgP2;
                            pi.playerCamera.rect = new Rect(0.5f, 0.5f, 1, 1);
                            break;
                        case 2:
                            pi = players[i].GetComponent<PlayerInfo>();
                            pi.playerRewiredID = 2;
                            pi.goldAmount = goldAmountTextP3;
                            pi.goldAmountImg = goldAmountImgP3;
                            pi.powerupImg = powerUpImgP3;
                            pi.playerCamera.rect = new Rect(0.5f, -0.5f, 1, 1);
                            break;
                        default:
                            break;
                    }
                }
                break;
            case 4:
                Debug.Log("4P");
                for (int i = 0; i < playerNumber; i++)
                {
                    switch (i)
                    {
                        case 0:
                            pi = players[i].GetComponent<PlayerInfo>();
                            pi.playerRewiredID = 0;
                            pi.goldAmount = goldAmountTextP1;
                            pi.goldAmountImg = goldAmountImgP1;
                            pi.powerupImg = powerUpImgP1;
                            pi.playerCamera.rect = new Rect(-0.5f, 0.5f, 1, 1);
                            break;
                        case 1:
                            pi = players[i].GetComponent<PlayerInfo>();
                            pi.playerRewiredID = 1;
                            pi.goldAmount = goldAmountTextP2;
                            pi.goldAmountImg = goldAmountImgP2;
                            pi.powerupImg = powerUpImgP2;
                            pi.playerCamera.rect = new Rect(0.5f, 0.5f, 1, 1);
                            break;
                        case 2:
                            pi = players[i].GetComponent<PlayerInfo>();
                            pi.playerRewiredID = 2;
                            pi.goldAmount = goldAmountTextP3;
                            pi.goldAmountImg = goldAmountImgP3;
                            pi.powerupImg = powerUpImgP3;
                            pi.playerCamera.rect = new Rect(0.5f, -0.5f, 1, 1);
                            break;
                        case 3:
                            pi = players[i].GetComponent<PlayerInfo>();
                            pi.playerRewiredID = 3;
                            pi.goldAmount = goldAmountTextP4;
                            pi.goldAmountImg = goldAmountImgP4;
                            pi.powerupImg = powerUpImgP4;
                            pi.playerCamera.rect = new Rect(-0.5f, -0.5f, 1, 1);
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }
    }


    IEnumerator LoadEndScene(string playScene, List<GameObject> players, bool copWin)
    {
        asyncLoadLevel = SceneManager.LoadSceneAsync(playScene, LoadSceneMode.Additive);

        while (!asyncLoadLevel.isDone)
        {
            print("Loading the Scene ... " + asyncLoadLevel.progress);
            yield return null;
        }
        winnerText = GameObject.FindGameObjectWithTag("WinnerText").GetComponent<Text>();
        if (copWin)
            winnerText.text = "Cop Win! All robbers jailed";
        else
            winnerText.text = "Time Over!";

        FindAndSortPanelPlayersByGoldAmount(players);

    }

    private void FindAndSortPanelPlayersByGoldAmount(List<GameObject> players)
    {
        IOrderedEnumerable<GameObject> playersSorted = from player in players orderby player.GetComponent<PlayerInfo>().goldSecured descending select player;

        SceneManager.SetActiveScene(SceneManager.GetSceneByName("EndMenu"));
        int nbPlayer = playersSorted.Count();

        ResultP1 = GameObject.FindGameObjectWithTag("Pos1");
        ResultP2 = GameObject.FindGameObjectWithTag("Pos2");
        ResultP3 = GameObject.FindGameObjectWithTag("Pos3");
        ResultP4 = GameObject.FindGameObjectWithTag("Pos4");

        switch (nbPlayer)
        {
            case 1:
                ResultP2.SetActive(false);
                ResultP3.SetActive(false);
                ResultP4.SetActive(false);
                break;
            case 2:
                ResultP3.SetActive(false);
                ResultP4.SetActive(false);
                break;
            case 3:
                ResultP4.SetActive(false);
                break;
            case 4:
                break;
            default:
                break;
        }

        if (ResultP1.activeInHierarchy)
        {
            ResultP1.transform.Find("GoldTotal").GetComponent<Text>().text = playersSorted.ElementAt(0).GetComponent<PlayerInfo>().goldSecured.ToString() + " $";
            ResultP1.transform.Find("PlayerName").GetComponent<Text>().color = playersSorted.ElementAt(0).GetComponent<PlayerInfo>().goldAmount.GetComponent<Text>().color;
            ResultP1.transform.Find("PlayerName").GetComponent<Text>().text = playersSorted.ElementAt(0).GetComponent<PlayerInfo>().playerName;
        }
        if (ResultP2.activeInHierarchy)
        {
            ResultP2.transform.Find("GoldTotal").GetComponent<Text>().text = playersSorted.ElementAt(1).GetComponent<PlayerInfo>().goldSecured.ToString() + " $";
            ResultP2.transform.Find("PlayerName").GetComponent<Text>().color = playersSorted.ElementAt(1).GetComponent<PlayerInfo>().goldAmount.GetComponent<Text>().color;
            ResultP2.transform.Find("PlayerName").GetComponent<Text>().text = playersSorted.ElementAt(1).GetComponent<PlayerInfo>().playerName;
        }
        if (ResultP3.activeInHierarchy)
        {
            ResultP3.transform.Find("GoldTotal").GetComponent<Text>().text = playersSorted.ElementAt(2).GetComponent<PlayerInfo>().goldSecured.ToString() + " $";
            ResultP3.transform.Find("PlayerName").GetComponent<Text>().color = playersSorted.ElementAt(2).GetComponent<PlayerInfo>().goldAmount.GetComponent<Text>().color;
            ResultP3.transform.Find("PlayerName").GetComponent<Text>().text = playersSorted.ElementAt(2).GetComponent<PlayerInfo>().playerName;
        }
        if (ResultP4.activeInHierarchy)
        {
            ResultP4.transform.Find("GoldTotal").GetComponent<Text>().text = playersSorted.ElementAt(3).GetComponent<PlayerInfo>().goldSecured.ToString() + " $";
            ResultP4.transform.Find("PlayerName").GetComponent<Text>().color = playersSorted.ElementAt(3).GetComponent<PlayerInfo>().goldAmount.GetComponent<Text>().color;
            ResultP4.transform.Find("PlayerName").GetComponent<Text>().text = playersSorted.ElementAt(3).GetComponent<PlayerInfo>().playerName;
        }
    }
}


