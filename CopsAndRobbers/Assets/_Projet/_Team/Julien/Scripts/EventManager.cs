﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class EventManager
{
    // Game Action
    public static Action<String, int> OnGameStart;
    public static Action OnPlayerReachTarget;
    public static Action OnTimeOver;
    public static Action OnGameOver;
    public static Action OnShowScore;
    public static Action OnShowMenu;

    // Player Action
    public static Action OnIdle;
    public static Action OnWalk;
    public static Action<GameObject, float> OnRun;
    public static Action<GameObject> OnInvisible;
    public static Action<GameObject> OnVisible;
    public static Action<GameObject> OnGrabbed;
    public static Action<GameObject, Transform[]> OnJailed;
    public static Action<GameObject> OnEscaped;


    #region Game Events
    public static void GameStart(string playScene, int playerNumber)
    {
        if (OnGameStart != null)
            OnGameStart(playScene, playerNumber);
    }

    public static void PlayerReachTarget()
    {
        if (OnPlayerReachTarget != null)
            OnPlayerReachTarget();
    }

    public static void TimeOver()
    {
        if (OnTimeOver != null)
            OnTimeOver();
    }

    public static void GameOver()
    {
        if (OnGameOver != null)
            OnGameOver();
    }

    public static void ShowScore()
    {
        if (OnShowScore != null)
            OnShowScore();
    }

    public static void ShowMenu()
    {
        if (OnShowMenu != null)
            OnShowMenu();
    }
    #endregion

    #region Player Event
    public static void Idle()
    {
        if (OnIdle != null)
            OnIdle();
    }

    public static void Walk()
    {
        if (OnWalk != null)
            OnWalk();
    }

    public static void Run(GameObject player, float speedMultiplier)
    {
        if (OnRun != null)
            OnRun(player, speedMultiplier);
    }

    public static void Invisible(GameObject player)
    {
        if (OnInvisible != null)
            OnInvisible(player);
    }

    public static void Visible(GameObject player)
    {
        if (OnInvisible != null)
            OnInvisible(player);
    }

    public static void Grabbed(GameObject player)
    {
        if (OnGrabbed != null)
            OnGrabbed(player);
    }

    public static void Jailed(GameObject player, Transform[] jailStart)
    {
        if (OnJailed != null)
            OnJailed(player,jailStart);
    }

    public static void Escaped(GameObject player)
    {
        if (OnEscaped != null)
            OnEscaped(player);
    }
    #endregion
}