﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleUp : MonoBehaviour
{

    public float powerUpDuration = 3;
    public bool randomStartPostision = false;


    private List<GameObject> playerMesh = new List<GameObject>();

    private void Start()
    {
        if (randomStartPostision)
        {

            PowerUpsManager.instance.SpawnRandomly(gameObject);
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine("BecomeInvisible", other);
        }
    }

    IEnumerator BecomeInvisible(Collider player)
    {
        PowerUpsManager.instance.DisablePowerUp(gameObject);

        EventManager.OnInvisible(player.gameObject);

        yield return new WaitForSeconds(powerUpDuration);

        EventManager.OnVisible(player.gameObject);

        PowerUpsManager.instance.EnablePowerUp(gameObject);
        PowerUpsManager.instance.SpawnRandomly(gameObject);
    }
}
