﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InvisibleWhenStatic : MonoBehaviour
{

    public float timeBeforeHidden = 2;
    public GameObject visibleIcon;
    public Material hiddenMat;
    public Material visibleMat;
    private Vector3 startPosition;
    private Vector3 endPosition;


    // Use this for initialization
    IEnumerator Start()
    {

        while (true)
        {
            startPosition = transform.position;
            yield return new WaitForSeconds(timeBeforeHidden);
            endPosition = transform.position;

            if (startPosition == endPosition)
            {
                EventManager.OnInvisible(this.gameObject);
            }
            else
            {
                transform.gameObject.GetComponent<MeshRenderer>().material = visibleMat;
                transform.gameObject.layer = LayerMask.NameToLayer("Player");
                visibleIcon.GetComponent<Image>().enabled = true;
            }
        }
    }
}
