﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRandomly : MonoBehaviour
{
    public Transform maxPoint;
    public Transform minPoint;
    public LayerMask layerMask;
    public LayerMask layerMaskToAvoid;
    public bool needFixPosY;
    public float fixPosY = 0.5f;

    private bool isPositionCorrect = false;

    // Use this for initialization
    void Start()
    {
        while (!isPositionCorrect)
        {
            Vector3 origin = new Vector3(Random.Range(minPoint.position.x, maxPoint.position.x), 1, Random.Range(minPoint.position.z, maxPoint.position.z));
            RaycastHit hit;

            if (Physics.Raycast(origin, Vector3.down, out hit, Mathf.Infinity, layerMask))
            {
                if (!Physics.Raycast(origin, Vector3.down, Mathf.Infinity, layerMaskToAvoid))
                {
                    isPositionCorrect = true;
                    transform.position = hit.point;
                    if (needFixPosY)
                    {
                        transform.position = new Vector3(hit.point.x, hit.point.y + fixPosY, hit.point.z);
                    }
                }
            }
        }
    }
}
