﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRTeleporter : MonoBehaviour {
    public GameObject teleportMarker;
    public GameObject VRPlayer;
    public float maxDistanceTeleport = 5f;
    public LayerMask teleportLayer;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray,out hit,maxDistanceTeleport,teleportLayer))
        {
            if (!teleportMarker.activeSelf)
            {
                teleportMarker.SetActive(true);
            }

            teleportMarker.transform.position = hit.point;
        }
        else
        {
            teleportMarker.SetActive(false);
        }

        if (OVRInput.GetDown(OVRInput.Button.SecondaryIndexTrigger) && teleportMarker.activeSelf)
        {
            VRPlayer.transform.position = new Vector3(teleportMarker.transform.position.x, VRPlayer.transform.position.y, teleportMarker.transform.position.z);
        }
	}
}
