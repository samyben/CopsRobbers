﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using UnityEngine.EventSystems;

public class ButtonManager : MonoBehaviour
{

    private Player player; // The Rewired Player
    public int playerId = 0; // The Rewired player id of this character
    public GameObject selectedObject;
    public Text playerNumber;
    public GameObject pauseCanvas;

    private EventSystem _eventSystem;

    void Awake()
    {
        // Get the Player for a particular playerId
        player = ReInput.players.GetPlayer(playerId);
    }

    private void Start()
    {
        if (selectedObject != null)
        {
            EventSystem.current.SetSelectedGameObject(selectedObject);
            selectedObject.GetComponent<Button>().OnSelect(null);
        }
    }

    private void Update()
    {
        if (selectedObject == null)
            selectedObject = GameObject.FindGameObjectWithTag("SelectedUI");

        if (!EventSystem.current.currentSelectedGameObject && (player.GetAxis("UIVertical") != 0 || player.GetAxis("UIHorizontal") != 0))
            EventSystem.current.SetSelectedGameObject(selectedObject);
    }

    public void MainMenuBtn(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void StartBtn(string playScene)
    {
        EventManager.GameStart(playScene, int.Parse(playerNumber.text));
    }

    public void ExitBtn()
    {
        Application.Quit();
    }

    public void EndGameRestartBtn(string sceneName)
    {
        EventManager.OnGameStart(sceneName, GameManager.instance.players.Count);
    }

    public void InGameRestartBtn()
    {
        string name = SceneManager.GetActiveScene().name;
        EventManager.OnGameStart(name, GameManager.instance.players.Count);
    }

    public void ResumeBtn()
    {
        pauseCanvas.SetActive(false);
        Time.timeScale = 1;
    }
}
