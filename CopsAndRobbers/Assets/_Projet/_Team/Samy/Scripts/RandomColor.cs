﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColor : MonoBehaviour
{
    private Renderer _renderer;
    private MaterialPropertyBlock _propBlock;

    void Awake()
    {
        _propBlock = new MaterialPropertyBlock();
        _renderer = GetComponent<Renderer>();
    }

    private void Start()
    {
        _renderer.GetPropertyBlock(_propBlock);
        _propBlock.SetColor("_Color", Random.ColorHSV(0, 1, 0.75f, 1, 0.75f, 1));
        _renderer.SetPropertyBlock(_propBlock);
    }
}
