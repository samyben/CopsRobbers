﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UsePowerUp : MonoBehaviour
{
    private Player rewiredPlayer; // The Rewired Player
    public int rewiredPlayerId; // The Rewired player id of this character
    public float powerUpDuration = 3;
    public float speedMultiplier = 2;

    private Image playerPowerUpImg;

    void Awake()
    {
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        rewiredPlayerId = GetComponent<PlayerInfo>().playerRewiredID;
        rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);
    }

    // Update is called once per frame
    void Update()
    {
        CheckIfRewiredPlayerIDChange();

        if (rewiredPlayer.GetButtonDown("UsePowerUp"))
        {
            playerPowerUpImg = GetComponent<PlayerInfo>().powerupImg;
            //Increase Player Speed
            if (playerPowerUpImg.color == PowerUpsManager.instance.speedUp)
            {
                StartCoroutine("IncreaseSpeed");
            } //Decrease other Players Speed
            else if (playerPowerUpImg.color == PowerUpsManager.instance.speedDown)
            {
                foreach (GameObject player in GameManager.instance.players)
                {
                    if (player != gameObject)
                    {
                        StartCoroutine(DecreaseSpeed(player));
                    }
                }
            }//Player become invisible
            else if (playerPowerUpImg.color == PowerUpsManager.instance.invisibility)
            {
                StartCoroutine("BecomeInvisible");
            }

            playerPowerUpImg.color = PowerUpsManager.instance.noPowerUp;
            playerPowerUpImg.enabled = false;
            playerPowerUpImg.rectTransform.parent.transform.Find("Background").GetComponent<Image>().color = PowerUpsManager.instance.noPowerUp;
        }
    }

    private void CheckIfRewiredPlayerIDChange()
    {
        int newRewiredID = GetComponent<PlayerInfo>().playerRewiredID;
        if (rewiredPlayerId != newRewiredID)
        {
            rewiredPlayerId = newRewiredID;
            rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);
        }
    }

    IEnumerator BecomeInvisible()
    {

        EventManager.OnInvisible(gameObject);

        yield return new WaitForSeconds(powerUpDuration);

        EventManager.OnVisible(gameObject);
    }

    IEnumerator IncreaseSpeed()
    {

        EventManager.OnRun(gameObject,speedMultiplier);

        yield return new WaitForSeconds(powerUpDuration);

        GetComponent<Move>().moveSpeed = GetComponent<Move>().moveSpeedAtStart;
    }

    IEnumerator DecreaseSpeed(GameObject player)
    {
        float divisor = speedMultiplier / speedMultiplier / speedMultiplier;
        EventManager.OnRun(player, divisor);

        yield return new WaitForSeconds(powerUpDuration);

        player.GetComponent<Move>().moveSpeed = player.GetComponent<Move>().moveSpeedAtStart;
    }

}
