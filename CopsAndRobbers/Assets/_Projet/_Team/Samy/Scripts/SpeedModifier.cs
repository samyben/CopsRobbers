﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedModifier : MonoBehaviour
{
    public float speedMultiplier = 2;
    public float powerUpDuration = 3;
    public bool inversePowerUp = false;
    public bool randomStartPostision = false;
    

    private void Start()
    {
        if (inversePowerUp == true)
            speedMultiplier = speedMultiplier / speedMultiplier / speedMultiplier;
        if (randomStartPostision)
            PowerUpsManager.instance.SpawnRandomly(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            StartCoroutine("ModifySpeed", other);
        }
    }

    IEnumerator ModifySpeed(Collider other)
    {
        PowerUpsManager.instance.DisablePowerUp(gameObject);
        EventManager.OnRun(other.gameObject, speedMultiplier);

        yield return new WaitForSeconds(powerUpDuration);
        
        other.GetComponent<Move>().moveSpeed = other.GetComponent<Move>().moveSpeedAtStart;

        PowerUpsManager.instance.EnablePowerUp(gameObject);
        PowerUpsManager.instance.SpawnRandomly(gameObject);
    }
}
