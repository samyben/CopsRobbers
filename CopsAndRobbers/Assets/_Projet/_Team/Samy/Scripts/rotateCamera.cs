﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotateCamera : MonoBehaviour
{

    private Player player; // The Rewired Player
    public int rewiredPlayerId; // The Rewired player id of this character
    public Transform playerHead;
    public bool useLimit = true;
    public int topAngleX = -85;
    public int bottomAngleX = 60;
    private float verticalPourcent;
    private float horizontalPourcent;
    public float cameraRotationSpeed = 5f;

    void Awake()
    {
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        rewiredPlayerId = GetComponent<PlayerInfo>().playerRewiredID;
        player = ReInput.players.GetPlayer(rewiredPlayerId);
    }

    void FixedUpdate()
    {
        CheckIfRewiredPlayerIDChange();

        if (player.GetAxis("RotateCameraHorizontal") != 0 || player.GetAxis("RotateCameraVertical") != 0)
        {

            horizontalPourcent = transform.localEulerAngles.y + (player.GetAxis("RotateCameraHorizontal") * cameraRotationSpeed);
            verticalPourcent += player.GetAxis("RotateCameraVertical") * cameraRotationSpeed;

            if (useLimit)
                verticalPourcent = Mathf.Clamp(verticalPourcent, topAngleX, bottomAngleX);

            transform.localRotation = Quaternion.Euler(0, horizontalPourcent, 0);
            playerHead.transform.localRotation = Quaternion.Euler(verticalPourcent, 0, 0);
        }
    }

    private void CheckIfRewiredPlayerIDChange()
    {
        int newRewiredID = GetComponent<PlayerInfo>().playerRewiredID;
        if (rewiredPlayerId != newRewiredID)
        {
            rewiredPlayerId = newRewiredID;
            player = ReInput.players.GetPlayer(rewiredPlayerId);
        }
    }
}
