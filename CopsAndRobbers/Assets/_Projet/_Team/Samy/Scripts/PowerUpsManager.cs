﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpsManager : MonoBehaviour
{

    public static PowerUpsManager instance = null;

    public Color noPowerUp;
    public Color speedUp;
    public Color speedDown;
    public Color invisibility;

    public Transform maxPoint;
    public Transform minPoint;
    public float positionYToSpawn = 0.05f;
    public LayerMask layerMask;
    public LayerMask layerMaskToAvoid;

    private bool isPositionCorrect = false;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    private void OnLevelWasLoaded(int level)
    {
        if (maxPoint == null || minPoint == null)
        {
            maxPoint = GameObject.FindGameObjectWithTag("MaxPoint").transform;
            minPoint = GameObject.FindGameObjectWithTag("MinPoint").transform;
        }
    }

    public void SpawnRandomly(GameObject powerUp)
    {
        while (!isPositionCorrect)
        {
            Vector3 origin = new Vector3(Random.Range(minPoint.position.x, maxPoint.position.x), 50, Random.Range(minPoint.position.z, maxPoint.position.z));
            RaycastHit hit;
            if (Physics.Raycast(origin, Vector3.down, out hit, Mathf.Infinity, layerMask))
            {
                if (!Physics.Raycast(origin, Vector3.down, Mathf.Infinity, layerMaskToAvoid))
                {
                    isPositionCorrect = true;
                    powerUp.transform.position = new Vector3(hit.point.x, hit.point.y + positionYToSpawn, hit.point.z);
                }
            }
        }
        isPositionCorrect = false;
    }

    public void DisablePowerUp(GameObject powerUp)
    {
        powerUp.GetComponent<MeshRenderer>().enabled = false;
        powerUp.GetComponent<BoxCollider>().enabled = false;
    }
    public void EnablePowerUp(GameObject powerUp)
    {

        powerUp.GetComponent<MeshRenderer>().enabled = true;
        powerUp.GetComponent<BoxCollider>().enabled = true;
    }
}
