﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class CheckIfIsOnNavmesh : MonoBehaviour
{

    public LayerMask layerMask;
    public float raycastLength = 0.1f;
    private IAInfo infosIA;

    private void Start()
    {
        infosIA = GetComponent<IAInfo>();
    }

    // Update is called once per frame
    void Update()
    {
        if (infosIA.isGrabbed)
        {
            GetComponent<MoveTo>().enabled = false;
            GetComponent<NavMeshAgent>().enabled = false;
            GetComponent<Rigidbody>().isKinematic = true;
        }
        else
        {
            GetComponent<Rigidbody>().isKinematic = false;
            if (!GetComponent<MoveTo>().enabled && !GetComponent<NavMeshAgent>().enabled)
            {
                RaycastHit hit;
                // Does the ray intersect any objects excluding the player layer
                if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, raycastLength, layerMask)
                    || Physics.Raycast(transform.position, transform.TransformDirection(Vector3.back), out hit, raycastLength, layerMask)
                    || Physics.Raycast(transform.position, transform.TransformDirection(Vector3.up), out hit, raycastLength, layerMask)
                    || Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, raycastLength, layerMask))
                {
                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow);
                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.back) * hit.distance, Color.yellow);
                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * hit.distance, Color.yellow);
                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * hit.distance, Color.yellow);
                    GetComponent<MoveTo>().enabled = true;
                    GetComponent<NavMeshAgent>().enabled = true;
                }
                else
                {

                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * 1000, Color.red);
                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.back) * 1000, Color.red);
                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.up) * 1000, Color.red);
                    //Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * 1000, Color.red);
                }
            }
        }
    }
}

