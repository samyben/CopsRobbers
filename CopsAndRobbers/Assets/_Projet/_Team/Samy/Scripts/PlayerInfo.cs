﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PostProcessing;
using UnityEngine.UI;

public class PlayerInfo : MonoBehaviour
{
    public string playerName;
    public GameObject body;
    public GameObject hair;
    public GameObject beard;
    public GameObject axe;
    public GameObject bag;
    public GameObject usableWeapon;
    public Camera playerCamera;
    public Material invisibleMat;
    public Text goldAmount;
    public Image goldAmountImg;
    public Image powerupImg;
    public ParticleSystem goldCoin;
    public AudioClip pickGoldclip;
    public AudioClip dropGoldclip;
    public ParticleSystem particule;

    public int playerRewiredID;
    [HideInInspector] public Material bodyMat;
    [HideInInspector] public Material hairMat;
    [HideInInspector] public Material beardMat;
    [HideInInspector] public Material axeMat;
    [HideInInspector] public Material bagMat;
    [HideInInspector] public bool isBoosted = false;
    [HideInInspector] public bool isStealing = false;
    [HideInInspector] public bool isSavingGold = false;
    [HideInInspector] public bool isJailed = false;
    public int maxGoldCapacity = 250;
    public int goldOnPlayer = 0;
    public int goldSecured;
    public int jailedCount = 0;
    public bool isKO = false;

    private int moneyTemp;

    private void Start()
    {
        bodyMat = body.GetComponent<SkinnedMeshRenderer>().material;
        bagMat = bag.GetComponent<SkinnedMeshRenderer>().material;
        hairMat = hair.GetComponent<MeshRenderer>().material;
        beardMat = beard.GetComponent<MeshRenderer>().material;
        axeMat = axe.GetComponent<MeshRenderer>().material;

        PostProcessingProfile ppProfile = playerCamera.GetComponent<PostProcessingBehaviour>().profile;
        VignetteModel.Settings vignetteSettings = ppProfile.vignette.settings;
        vignetteSettings.intensity = 0f;
        ppProfile.vignette.settings = vignetteSettings;

        moneyTemp = goldOnPlayer;
    }

    private void Update()
    {
        if (moneyTemp != goldOnPlayer)
        {
            float ratio = (float)goldOnPlayer / (float)maxGoldCapacity;
            goldAmountImg.fillAmount = 1 - ratio;
            moneyTemp = goldOnPlayer;
            Color32 emissiveColor = new Color(ratio, ratio, ratio, 0);
            bagMat.SetColor("_Emission", emissiveColor);
        }
    }

}
