﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInJail : MonoBehaviour
{
    public Transform[] jailStart;
    public Text jailPlayerNameText;
    public float messageDuration = 1.5f;

    public Transform maxPoint;
    public Transform minPoint;
    public LayerMask layerMask;
    public LayerMask layerMaskToAvoid;

    private bool allPlayersJailed = false;
    private bool isPositionCorrect = false;

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            EventManager.OnJailed(other.gameObject, jailStart);
            StartCoroutine(EnableJailCanvas(other.gameObject));

            if (GameManager.instance.players.Count > 1)
            {
                foreach (GameObject player in GameManager.instance.players)
                {
                    if (player.GetComponent<PlayerInfo>().isJailed)
                        allPlayersJailed = true;
                    else
                    {
                        allPlayersJailed = false;
                        return;
                    }
                }

                if (allPlayersJailed)
                    EventManager.OnGameOver();
            }
        }
        else if (other.tag == "IA")
        {
            other.transform.position = jailStart[Random.Range(0, jailStart.Length)].position;
            other.GetComponent<IAInfo>().isGrabbed = false;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        StopCoroutine("EnableJailCanvas");
    }

    IEnumerator EnableJailCanvas(GameObject player)
    {
        jailPlayerNameText.text = player.GetComponent<PlayerInfo>().playerName;
        jailPlayerNameText.color = player.GetComponent<PlayerInfo>().goldAmount.color;
        jailPlayerNameText.rectTransform.parent.gameObject.SetActive(true);

        yield return new WaitForSeconds(messageDuration);

        jailPlayerNameText.rectTransform.parent.gameObject.SetActive(false);
    }

}
