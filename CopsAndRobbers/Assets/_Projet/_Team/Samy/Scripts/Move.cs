﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{

    private Player rewiredPlayer; // The Rewired Player
    public int rewiredPlayerId; // The Rewired player id of this character

    public float moveSpeed = 3.0f;
    public bool enableSprintTrail = true;
    public bool isKeyboardPlayer = false;

    private Rigidbody playerRigidbody;
    private Vector3 moveVector = new Vector3();
    private Animation anim;
    [HideInInspector] public float moveSpeedAtStart;

    void Awake()
    {
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        rewiredPlayerId = GetComponent<PlayerInfo>().playerRewiredID;
        rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);
    }

    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        anim = GetComponent<Animation>();
        moveSpeedAtStart = moveSpeed;
    }

    private void FixedUpdate()
    {
        CheckIfRewiredPlayerIDChange();

        if (isKeyboardPlayer)   //Move Player with Keyboard
        {
            if (Input.GetKey(KeyCode.Z))
            {
                playerRigidbody.MovePosition(playerRigidbody.position + transform.forward * moveSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                playerRigidbody.MovePosition(playerRigidbody.position + transform.forward * -moveSpeed * Time.deltaTime);
            }

            if (Input.GetKey(KeyCode.Q))
            {
                playerRigidbody.MovePosition(playerRigidbody.position + transform.right * -moveSpeed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.D))
            {
                playerRigidbody.MovePosition(playerRigidbody.position + transform.right * moveSpeed * Time.deltaTime);
            }

            //enable & disable animation
            if (Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.D))
            {
                StartWalk();
            }
            else if (!Input.GetKey(KeyCode.Z) && !Input.GetKey(KeyCode.S) && !Input.GetKey(KeyCode.Q) && !Input.GetKey(KeyCode.D))
            {
                StartIdle();
            }
        }
        else if (!isKeyboardPlayer && rewiredPlayer != null)     //Move Player with GamePad
        {
            moveVector.x = rewiredPlayer.GetAxis("MoveHorizontal");
            moveVector.z = rewiredPlayer.GetAxis("MoveVertical");
            if (moveVector.z != 0.0f)
            {
                playerRigidbody.MovePosition(playerRigidbody.position + transform.forward * moveVector.z * moveSpeed * Time.deltaTime);
            }
            if (moveVector.x != 0.0f)
            {
                playerRigidbody.MovePosition(playerRigidbody.position + transform.right * moveVector.x * moveSpeed * Time.deltaTime);
            }

            //enable & disable animation
            if (moveVector.x == 0.0f && moveVector.y == 0.0f)
            {
                StartIdle();
            }
            else if (moveVector.x != 0.0f || moveVector.y != 0.0f)
            {
                StartWalk();
            }
        }
    }

    private void CheckIfRewiredPlayerIDChange()
    {
        int newRewiredID = GetComponent<PlayerInfo>().playerRewiredID;
        if (rewiredPlayerId != newRewiredID)
        {
            rewiredPlayerId = newRewiredID;
            rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);
        }
    }

    private void StartIdle()
    {
        if (anim != null && !anim.IsPlaying("Lumbering") && !anim.IsPlaying("Idle"))
        {
            Debug.Log("StartIdle => " + anim.IsPlaying("Idle"));
            anim.clip = anim.GetClip("Idle");
            anim.Play();
        }
    }

    private void StartWalk()
    {
        if (anim != null && !anim.IsPlaying("Lumbering") && !anim.IsPlaying("Walk"))
        {
            Debug.Log("StartWalk => " + anim.IsPlaying("Walk"));
            anim.clip = anim.GetClip("Walk");
            anim.Play();
        }
    }
}
