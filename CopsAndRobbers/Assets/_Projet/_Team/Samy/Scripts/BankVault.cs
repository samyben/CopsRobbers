﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BankVault : MonoBehaviour
{

    private PlayerInfo playerInfo;


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            other.GetComponent<PlayerInfo>().isStealing = true; ;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
            other.GetComponent<PlayerInfo>().isStealing = false; 
    }
}
