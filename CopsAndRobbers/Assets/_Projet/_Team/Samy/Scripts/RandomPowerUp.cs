﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomPowerUp : MonoBehaviour
{
    public float timeBetweenColorChange = 0.2f;

    private int index = 0;
    private List<Color> colors = new List<Color>();

    public float countdown;
    private Renderer _renderer;
    private AudioSource audioSource;

    void Awake()
    {
        _renderer = GetComponent<Renderer>();
    }

    // Use this for initialization
    void Start()
    {
        countdown = timeBetweenColorChange;

        colors.Add(PowerUpsManager.instance.speedUp);
        colors.Add(PowerUpsManager.instance.invisibility);

        //add multiplayer Powerups
        if (GameManager.instance.numberOfPlayer > 1)
        {
            colors.Add(PowerUpsManager.instance.speedDown);
        }
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (countdown > 0)
        {
            countdown -= Time.deltaTime;
        }
        else
        {
            countdown = timeBetweenColorChange;

            _renderer.material.EnableKeyword("_EMISSION");
            _renderer.material.SetColor("_EmissionColor", colors[index]);

            if (index >= colors.Count - 1)
            {
                index = 0;
            }
            else
            {
                index++;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.tag == "Player")
        {
            PowerUpsManager.instance.SpawnRandomly(gameObject);

            audioSource.Play();

            Image powerUpImg = other.GetComponent<PlayerInfo>().powerupImg;
            powerUpImg.enabled = true;

            //Change powerup color
            if (_renderer.material.GetColor("_EmissionColor") == PowerUpsManager.instance.speedUp)
            {
                powerUpImg.sprite = Resources.Load<Sprite>("Images/UpSpeedArrows");
                powerUpImg.color = PowerUpsManager.instance.speedUp;
                powerUpImg.rectTransform.parent.transform.Find("Background").GetComponent<Image>().color = PowerUpsManager.instance.speedUp;
            }
            else if (_renderer.material.GetColor("_EmissionColor") == PowerUpsManager.instance.speedDown)
            {
                powerUpImg.sprite = Resources.Load<Sprite>("Images/DownSpeedArrows");
                powerUpImg.color = PowerUpsManager.instance.speedDown;
                powerUpImg.rectTransform.parent.transform.Find("Background").GetComponent<Image>().color = PowerUpsManager.instance.speedDown;
            }
            else if (_renderer.material.GetColor("_EmissionColor") == PowerUpsManager.instance.invisibility)
            {
                powerUpImg.sprite = Resources.Load<Sprite>("Images/InvisibilityCloak");
                powerUpImg.color = PowerUpsManager.instance.invisibility;
                powerUpImg.rectTransform.parent.transform.Find("Background").GetComponent<Image>().color = PowerUpsManager.instance.invisibility;
            }
        }
    }
}