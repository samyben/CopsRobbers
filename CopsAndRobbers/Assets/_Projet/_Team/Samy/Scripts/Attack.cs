﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour
{
    private Player rewiredPlayer; // The Rewired Player
    public int rewiredPlayerId; // The Rewired player id of this character
    private Animation anim;
    private PlayerInfo pi;

    void Awake()
    {
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        rewiredPlayerId = GetComponent<PlayerInfo>().playerRewiredID;
        rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);
    }

    void Start()
    {
        anim = GetComponent<Animation>();
        pi = GetComponent<PlayerInfo>();
        pi.usableWeapon.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        CheckIfRewiredPlayerIDChange();

        if (rewiredPlayer.GetButtonDown("Attack"))
        {
           StartCoroutine(PlayerAttack());
        }
    }
    IEnumerator PlayerAttack()
    {
        if (anim != null && !anim.IsPlaying("Lumbering"))
        {
            pi.usableWeapon.SetActive(true);
            anim.clip = anim.GetClip("Lumbering");
            anim.Play();
            yield return new WaitForSeconds(anim.clip.length);
            pi.usableWeapon.SetActive(false);
            anim.Stop();
        }
    }

    private void CheckIfRewiredPlayerIDChange()
    {
        int newRewiredID = pi.playerRewiredID;
        if (rewiredPlayerId != newRewiredID)
        {
            rewiredPlayerId = newRewiredID;
            rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);
        }
    }
}
