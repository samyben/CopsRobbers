﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.PostProcessing;

public class CheckIfHitPlayer : MonoBehaviour
{
    public float freezeDuration = 1.5f;
    private GameObject player;
    private GameObject playerAttacked;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectsWithTag("Player").Where(p => p.GetComponent<PlayerInfo>().usableWeapon == gameObject).FirstOrDefault();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && other.gameObject != player)
        {
            if (!other.GetComponent<PlayerInfo>().isKO)
            {
                Debug.Log("HitPlayer!!");
                playerAttacked = other.gameObject;
                PlayerInfo playerAttackedPi = playerAttacked.GetComponent<PlayerInfo>();
                PlayerInfo playerPi = player.GetComponent<PlayerInfo>();

                int motorIndex = 0; // the first motor
                float motorLevel = 1.0f; // full motor speed
                float duration = 0.3f; // 2 seconds

                ReInput.players.GetPlayer(player.GetComponent<PlayerInfo>().playerRewiredID).SetVibration(motorIndex, motorLevel, duration);
                PostProcessingProfile ppProfile = playerAttacked.GetComponent<PlayerInfo>().playerCamera.GetComponent<PostProcessingBehaviour>().profile;
                VignetteModel.Settings vignetteSettings = ppProfile.vignette.settings;
                vignetteSettings.intensity = 0.75f;
                ppProfile.vignette.settings = vignetteSettings;

                int money = playerAttackedPi.goldOnPlayer / 2;
                if (playerPi.goldOnPlayer < playerPi.maxGoldCapacity)
                {
                    if (playerPi.goldOnPlayer + money >= playerPi.maxGoldCapacity)
                    {
                        money = playerPi.maxGoldCapacity - playerPi.goldOnPlayer;
                    }
                }
                else
                {
                    money = 0;
                }

                playerAttackedPi.goldOnPlayer = playerAttackedPi.goldOnPlayer - money;
                playerPi.goldOnPlayer = playerPi.goldOnPlayer + money;
                playerAttackedPi.goldAmount.text = playerAttackedPi.goldOnPlayer.ToString();
                playerPi.goldAmount.text = playerPi.goldOnPlayer.ToString();
                
                playerAttackedPi.isKO = true;
                playerAttacked.GetComponent<Move>().enabled = false;
                playerAttacked.GetComponent<rotateCamera>().enabled = false;
                //playerAttacked.transform.localRotation = Quaternion.Euler(playerAttacked.transform.localRotation.x, playerAttacked.transform.localRotation.y, playerAttacked.transform.localRotation.z - 90);

                Invoke("UnfreezePlayer",freezeDuration);
            }
        }
    }

    private void UnfreezePlayer()
    {
        playerAttacked.GetComponent<PlayerInfo>().isKO = false;
        //playerAttacked.transform.localRotation = Quaternion.Euler(playerAttacked.transform.localRotation.x, playerAttacked.transform.localRotation.y, playerAttacked.transform.localRotation.z + 90);
        playerAttacked.GetComponent<rotateCamera>().enabled = true;
        playerAttacked.GetComponent<Move>().enabled = true;
        PostProcessingProfile ppProfile = playerAttacked.GetComponent<PlayerInfo>().playerCamera.GetComponent<PostProcessingBehaviour>().profile;
        VignetteModel.Settings vignetteSettings = ppProfile.vignette.settings;
        vignetteSettings.intensity = 0f;
        ppProfile.vignette.settings = vignetteSettings;
    }
}
