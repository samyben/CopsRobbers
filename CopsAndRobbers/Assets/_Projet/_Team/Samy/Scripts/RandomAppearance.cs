﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAppearance : MonoBehaviour
{
    public bool isIA = false;
    public bool randomBag = false;

    private PlayerInfo playerInfo;
    private IAInfo IAInfo;
    private GameObject hair;
    private GameObject beard;
    private GameObject axe;
    private GameObject bag;

    // Use this for initialization
    void Awake()
    {
        if (!isIA)
        {
            playerInfo = GetComponent<PlayerInfo>();
            hair = playerInfo.hair;
            beard = playerInfo.beard;
            axe = playerInfo.axe;
            bag = playerInfo.bag;
        }
        else
        {
            IAInfo = GetComponent<IAInfo>();
            hair = IAInfo.hair;
            beard = IAInfo.beard;
            axe = IAInfo.axe;
            bag = IAInfo.bag;
        }
    }

    void Start()
    {
        RandomActivateGameobject(hair);
        RandomActivateGameobject(beard);
        RandomActivateGameobject(axe);
        if (randomBag)
            RandomActivateGameobject(bag);
    }

    private void RandomActivateGameobject(GameObject go)
    {
        int random = Random.Range(0, 2);
        if (random == 0)
            go.SetActive(false);
        else
            go.SetActive(true);
    }
}
