﻿using Rewired;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TogglePause : MonoBehaviour
{
    private Player rewiredPlayer; // The Rewired Player
    public int rewiredPlayerId; // The Rewired player id of this character
    private bool isPaused = false;

    public GameObject pauseCanvas;

    void Awake()
    {
        // Get the Rewired Player object for this player and keep it for the duration of the character's lifetime
        rewiredPlayerId = GetComponent<PlayerInfo>().playerRewiredID;
        rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);
    }


    private void Update()
    {
        CheckIfRewiredPlayerIDChange();

        if (rewiredPlayer.GetButtonDown("Pause"))
        {
            isPaused = !isPaused;
            pauseCanvas.SetActive(isPaused);

            if (isPaused)
                Time.timeScale = 0;
            else
                Time.timeScale = 1;
        }
    }

    private void CheckIfRewiredPlayerIDChange()
    {
        int newRewiredID = GetComponent<PlayerInfo>().playerRewiredID;
        if (rewiredPlayerId != newRewiredID)
        {
            rewiredPlayerId = newRewiredID;
            rewiredPlayer = ReInput.players.GetPlayer(rewiredPlayerId);
        }
    }

}
