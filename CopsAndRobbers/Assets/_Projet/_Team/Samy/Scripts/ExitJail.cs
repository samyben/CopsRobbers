﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitJail : MonoBehaviour
{
    public Transform minPoint;
    public Transform maxPoint;
    public LayerMask layerMaskToSpawn;
    public LayerMask layerMaskToAvoid;

    private bool posIsCorrect = false;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && other.GetComponent<PlayerInfo>().isJailed)
        {
            other.gameObject.layer = LayerMask.NameToLayer("Player");
            posIsCorrect = false;
            while (!posIsCorrect)
            {
                Vector3 origin = new Vector3(Random.Range(minPoint.position.x, maxPoint.position.x), 50, Random.Range(minPoint.position.z, maxPoint.position.z));
                RaycastHit hit;
                if (Physics.Raycast(origin, Vector3.down, out hit, Mathf.Infinity, layerMaskToSpawn))
                {
                    if (!Physics.Raycast(origin, Vector3.down, Mathf.Infinity, layerMaskToAvoid))
                    {
                        posIsCorrect = true;
                        other.transform.position = hit.point;
                    }
                }
            }
            other.GetComponent<PlayerInfo>().isJailed = false;
        }
    }
}
