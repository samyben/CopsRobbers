﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NAudio;
using NAudio.Wave;
using NAudio.CoreAudioApi;

public class StealAndSaveMoney : MonoBehaviour
{

    private PlayerInfo playerInfo;
    public float setCountDown = 0.2f;
    public int minGoldGain = 3;
    public int maxGoldGain = 13;
    public float countDown;

    private AudioSource audioSource;

    private void Start()
    {
        playerInfo = GetComponent<PlayerInfo>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        //Stealing
        if (playerInfo.isStealing)
        {
            if (playerInfo.goldOnPlayer < playerInfo.maxGoldCapacity)
            {
                if (countDown > 0f)
                {
                    countDown -= Time.deltaTime;
                }

                if (countDown <= 0f)
                {
                    int random = Random.Range(minGoldGain, maxGoldGain);
                    playerInfo.goldOnPlayer = playerInfo.goldOnPlayer + random;
                    playerInfo.goldAmount.text = playerInfo.goldOnPlayer.ToString();

                    if (!audioSource.isPlaying)
                    {
                        audioSource.clip = playerInfo.pickGoldclip;
                        audioSource.Play();
                    }

                    countDown = setCountDown;
                }
            }
            else
            {
                playerInfo.goldOnPlayer = playerInfo.maxGoldCapacity;
                playerInfo.goldAmount.text = playerInfo.goldOnPlayer.ToString();
            }
        }
        
        //Saving
        if (playerInfo.goldOnPlayer != 0 && playerInfo.isSavingGold)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.clip = playerInfo.dropGoldclip;
                audioSource.Play();
            }
            EventManager.OnEscaped(gameObject);
        }
    }
}

