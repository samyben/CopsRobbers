﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MoveTo : MonoBehaviour
{
    private float minX;
    private float maxX;
    private float minZ;
    private float maxZ;
    public Transform minPoint;
    public Transform maxPoint;
    public float minTimeBetweenWait = 3f;
    public float maxTimeBetweenWait = 10f;
    public float minWaitingTime = 0.5f;
    public float maxWaitingTime = 3f;

    private Animation anim;
    public Vector3 goal;
    private NavMeshAgent agent;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
    }

    void Start()
    {
        minX = minPoint.position.x;
        maxX = maxPoint.position.x;
        minZ = minPoint.position.z;
        maxZ = maxPoint.position.z;
        anim = GetComponent<Animation>();
        goal = new Vector3(Random.Range(minX, maxX), minPoint.position.y, Random.Range(minZ, maxZ));
        agent.avoidancePriority = Random.Range(0, 99);
        agent.destination = goal;
        agent.updateRotation = false;
        StartWalk();
    }

    private void Update()
    {
        if (!GetComponent<IAInfo>().isGrabbed)
        {
            StartCoroutine("WaitAfter", agent);

            // Check if we've reached the destination
            if (!agent.pathPending)
            {
                if (agent.isActiveAndEnabled && agent.remainingDistance <= agent.stoppingDistance)
                {
                    if (!agent.hasPath || agent.velocity.sqrMagnitude == 0f)
                    {
                        goal = new Vector3(Random.Range(minX, maxX), minPoint.position.y, Random.Range(minZ, maxZ));
                        agent.destination = goal;
                        StartWalk();
                    }
                }
            }

        }
    }

    void LateUpdate()
    {
        if (agent.velocity.sqrMagnitude > Mathf.Epsilon)
        {
            transform.rotation = Quaternion.LookRotation(agent.velocity.normalized);
        }
    }

    IEnumerator WaitAfter(NavMeshAgent agent)
    {
        if (agent.pathPending)
        {
            float waitingTime = Random.Range(minTimeBetweenWait, maxTimeBetweenWait);
            yield return new WaitForSeconds(waitingTime);
            StartCoroutine("Wait", agent);
        }
    }

    IEnumerator Wait(NavMeshAgent agent)
    {
        if (agent.isActiveAndEnabled)
        {
            agent.isStopped = true;
            StartIdle();
            float waitingTime = Random.Range(minWaitingTime, maxWaitingTime);
            yield return new WaitForSeconds(waitingTime);
            agent.isStopped = false;
            StartWalk();
        }
    }

    private void StartIdle()
    {
        if (anim != null && !anim.IsPlaying("Idle"))
        {
            anim.clip = anim.GetClip("Idle");
            anim.Play();
        }
    }

    private void StartWalk()
    {
        if (anim != null && !anim.IsPlaying("Walk"))
        {
            anim.clip = anim.GetClip("Walk");
            anim.Play();
        }
    }
}
