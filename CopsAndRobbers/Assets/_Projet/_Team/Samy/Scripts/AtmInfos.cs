﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtmInfos : MonoBehaviour
{
    public Light atmLight;
    public int minAtmGold = 25;
    public int maxAtmGold = 101;
    public float atmOffDuration = 5;

    [HideInInspector]
    public bool isWorking = true;
    [HideInInspector]
    public int moneyInATM;

    private Color normalLightColor;
    private float normalLightRange;
    private float normalLightIntensity;
    private Animation alarmAnim;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        moneyInATM = Random.Range(minAtmGold, maxAtmGold);
        alarmAnim = GetComponent<Animation>();
        audioSource = GetComponent<AudioSource>();
        normalLightColor = atmLight.color;
        normalLightRange = atmLight.range;
        normalLightIntensity = atmLight.intensity;
    }

    public void ActivateAlarm(GameObject player)
    {
        StartCoroutine(AlarmOn(player));
    }

    IEnumerator AlarmOn(GameObject player)
    {
        PlayerInfo pi = player.GetComponent<PlayerInfo>();
        if (pi.goldOnPlayer + moneyInATM >= pi.maxGoldCapacity)
        {
            pi.goldOnPlayer = pi.maxGoldCapacity;
            Debug.Log("ATM Joueur FULL");
            //message trop d'argent sur le joueur
        }
        else
            pi.goldOnPlayer += moneyInATM;

        pi.goldAmount.text = pi.goldOnPlayer.ToString();
        isWorking = false;
        alarmAnim.Play();
        if (!audioSource.isPlaying)
        {
            audioSource.Play();
        }

        yield return new WaitForSeconds(alarmAnim.clip.length);
        StartCoroutine(DisableATM());
    }


    IEnumerator DisableATM()
    {
        if (atmLight != null)
        {
            atmLight.enabled = false;
            if (audioSource.isPlaying)
            {
                audioSource.Stop();
            }
        }

        yield return new WaitForSeconds(atmOffDuration);

        if (atmLight != null)
        {
            atmLight.color = normalLightColor;
            atmLight.range = normalLightRange;
            atmLight.intensity = normalLightIntensity;
            atmLight.enabled = true;
        }


        moneyInATM = Random.Range(minAtmGold, maxAtmGold);

        isWorking = true;
    }
}
