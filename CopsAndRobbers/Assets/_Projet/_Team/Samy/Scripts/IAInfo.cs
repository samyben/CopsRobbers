﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IAInfo : MonoBehaviour
{
    public GameObject body;
    public GameObject hair;
    public GameObject beard;
    public GameObject axe;
    public GameObject bag;
    public bool isGrabbed = false;
    
    [HideInInspector] public Material bodyMat;
    [HideInInspector] public Material hairMat;
    [HideInInspector] public Material beardMat;
    [HideInInspector] public Material axeMat;
    [HideInInspector] public Material bagMat;

    private void Start()
    {
        bodyMat = body.GetComponent<SkinnedMeshRenderer>().material;
        bagMat = bag.GetComponent<SkinnedMeshRenderer>().material;
        hairMat = hair.GetComponent<MeshRenderer>().material;
        beardMat = beard.GetComponent<MeshRenderer>().material;
        axeMat = axe.GetComponent<MeshRenderer>().material;
    }


}
