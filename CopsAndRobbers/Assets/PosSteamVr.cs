﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PosSteamVr : MonoBehaviour {

    public Transform pos;

	// Use this for initialization
	void Awake () {
        transform.position = pos.position;
        transform.rotation = pos.rotation;
        
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
