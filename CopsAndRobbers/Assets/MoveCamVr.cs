﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamVr : MonoBehaviour {

    public Transform camEyes;
    public float horizontalMultiplier = 10;
    public float verticalMultiplier = 5;
    public GameObject cameraRig;
    

    // Use this for initialization
    void Start () {
        cameraRig.transform.localRotation = new Quaternion(cameraRig.transform.localRotation.x, 0f, cameraRig.transform.localRotation.z, cameraRig.transform.localRotation.w);
        
    }

	
	// Update is called once per frame
	void Update () {
        transform.localPosition = new Vector3(camEyes.localPosition.x * horizontalMultiplier, camEyes.localPosition.y - 0.1f * verticalMultiplier, camEyes.localPosition.z * horizontalMultiplier);
	}
}
