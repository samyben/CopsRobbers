﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeteJail : MonoBehaviour {

    public SphereCasterTete sphereCasterTete;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        sphereCasterTete.DepJail = true;
    }
}
