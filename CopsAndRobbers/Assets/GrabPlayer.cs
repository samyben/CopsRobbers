﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabPlayer : MonoBehaviour
{

    //private Valve.VR.EVRButtonId gripButton = Valve.VR.EVRButtonId.k_EButton_Grip;
    //private Valve.VR.EVRButtonId triggerButton = Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger;

    //private SteamVR_Controller.Device controller { get { return SteamVR_Controller.Input((int)trackedObj.index);  } }
    public SteamVR_TrackedObject trackedObj;

    private GameObject pickup;

    SteamVR_Controller.Device left;
    public float grabRadius = 0.1f;

    public float playerInfoGold;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (left == null)
        {
            if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                return;

            left = SteamVR_Controller.Input((int)trackedObj.index);
        }

        if (pickup == null && left.GetPressDown(SteamVR_Controller.ButtonMask.Grip))
        {
            foreach (var item in Physics.OverlapSphere(transform.position, grabRadius))
            {
                if (item.tag == "Player")
                {
                    pickup = item.gameObject;
                    pickup.transform.parent = this.transform;
                    playerInfoGold = pickup.GetComponent<PlayerInfo>().goldOnPlayer;
                    if (playerInfoGold > 0)
                    {
                        Debug.Log("AAAAAAAAAAAAA" + playerInfoGold);
                        PlayerManager.instance.Grabbed(pickup);
                    }
                }
                if (item.tag == "IA")
                {
                    pickup = item.gameObject;
                    pickup.transform.parent = this.transform;
                }
            }
        }

        if (pickup != null && left.GetPress(SteamVR_Controller.ButtonMask.Grip))
        {
            pickup.GetComponent<Rigidbody>().isKinematic = true;
            pickup.transform.position = this.transform.position;
            pickup.transform.rotation = this.transform.rotation;
            if (pickup.tag == "Player")
            {
                // particle.Play();
                //Debug.Log("Particule");
            }
            //new Vector3(this.transform.position.x, (this.transform.position.y + 1), this.transform.position.z);
            Debug.Log("Grip Button was just pressed ");

        }

        if (pickup != null && left.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        {
            pickup.transform.SetParent(null);
            pickup.GetComponent<Rigidbody>().isKinematic = false;
            if (pickup.tag == "Player")
                pickup.GetComponent<PlayerInfo>().goldCoin.Stop();

            pickup = null;

            Debug.Log("Grip Button was just unpressed");
        }
    }

    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.tag == "Player")
    //    {
    //        Debug.Log("Trigged Entered");
    //        pickup = other.gameObject;
    //    }
    //}

    //private void OnTriggerExit(Collider other)
    //{
    //    pickup = null;
    //    Debug.Log("Trigged exit");
    //}


}
