﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabPlayerOVR : MonoBehaviour
{
    public OVRInput.Button grabButton;
    public float grabRadius = 0.1f;

    private GameObject pickup;
    public float playerInfoGold;

    // Update is called once per frame
    void Update()
    {
        if (pickup == null && OVRInput.GetDown(grabButton))
        {
            foreach (var item in Physics.OverlapSphere(transform.position, grabRadius))
            {
                if (item.tag == "Player")
                {
                    pickup = item.gameObject;
                    pickup.transform.parent = this.transform;
                    playerInfoGold = pickup.GetComponent<PlayerInfo>().goldOnPlayer;
                }
                if (item.tag == "IA")
                {
                    pickup = item.gameObject;
                    pickup.transform.parent = this.transform;
                }
            }
        }

        if (pickup != null && OVRInput.Get(grabButton))
        {
            pickup.GetComponent<Rigidbody>().isKinematic = true;
            pickup.transform.position = this.transform.position;
            pickup.transform.rotation = this.transform.rotation;
            PlayerManager.instance.Grabbed(pickup);
            Debug.Log("Grip Button was just pressed ");
        }

        if (pickup != null && OVRInput.GetUp(grabButton))
        {
            pickup.transform.SetParent(null);
            pickup.GetComponent<Rigidbody>().isKinematic = false;
            if (pickup.tag == "Player")
            {
                if (pickup.GetComponent<PlayerInfo>().goldCoin.isPlaying)
                {
                    pickup.GetComponent<PlayerInfo>().goldCoin.Stop();
                }
            }

            pickup = null;

            Debug.Log("Grip Button was just unpressed");
        }
    }
}
