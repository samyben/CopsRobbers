﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckIfUnderRoof : MonoBehaviour {

    public Transform torchLight;
    public LayerMask layer;

    private Vector3 origin;

    private float currentHitDistance;

    // Update is called once per frame
    void Update () {
        RaycastHit hit;
        origin = transform.position;
        Debug.Log(torchLight.position);

        Vector3 dir = (torchLight.position - transform.position).normalized;
        if (Physics.Raycast(origin, dir, out hit, Mathf.Infinity, layer))
        {
            currentHitDistance = hit.distance;
            gameObject.layer = 15;
        }
        else
        {
            if (gameObject.tag == "Player" )
            {
                gameObject.layer = 10;
            }
            else
            {
                gameObject.layer = 14;
            }
        }

        if (Physics.Raycast(origin, Vector3.down, out hit, Mathf.Infinity, layer))
        {

            Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.down) * hit.distance, Color.yellow);
            if (gameObject.tag == "Player")
            {
                gameObject.layer = 10;
            }
            else
            {
                gameObject.layer = 14;
            }
        }

    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(origin, origin + torchLight.position * currentHitDistance);
        Gizmos.DrawLine(origin, torchLight.position * currentHitDistance);
    }
}
