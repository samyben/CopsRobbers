﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NAudio;
using NAudio.Wave;

public class Naudio : MonoBehaviour
{
    WaveOut waveOut;
    WaveFileReader waveReader;
    WaveFileReader waveReader2;
    private NAudio.Wave.DirectSoundOut output = null;

    // Start is called before the first frame update
    void Start()
    {
        waveOut = new WaveOut();
        Debug.Log("NAUDIO device " + waveOut.DeviceNumber);
        Debug.Log("NAUDIO Count " + WaveOut.DeviceCount);

        for (int i = -1; i < WaveOut.DeviceCount; i++)
        {
            var caps = WaveOut.GetCapabilities(i);
            Debug.Log("Device Name: " + caps.ProductName);
        }
        disposeWave();
        waveOut = new WaveOut();
        waveReader = new WaveFileReader("Assets/_Projet/Resources/Sounds/PowerUp.wav");
        waveOut.DeviceNumber = -1;
        waveOut.Init(waveReader);
        waveOut.Play();

        Invoke("s",0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void s()
    {
        disposeWave();
        waveOut = new WaveOut();
        waveReader2 = new WaveFileReader("Assets/_Projet/Resources/Sounds/PowerUp2.wav");
        waveOut.DeviceNumber = 1;
        waveOut.Init(waveReader2);
        waveOut.Play();
    }

    public void disposeWave()
    {
        if (output != null)
        {
            if (output.PlaybackState == PlaybackState.Playing)
            {
                output.Stop();
                output.Dispose();
                output = null;
            }
        }
        if (waveOut != null)
        {
            waveOut.Dispose();
            waveOut = null;
        }
    }
}
