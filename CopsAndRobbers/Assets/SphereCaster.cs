﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class SphereCaster : MonoBehaviour
{

    public SteamVR_TrackedObject trackedObj;

    public GameObject currentHitObject;

    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;
    public Animator animator;


    private Vector3 origin;
    private Vector3 direction;

    private float currentHitDistance;

    SteamVR_Controller.Device right;

    public float wait;

    public float setCountDown = 2f;
    public float countDown;

    public Text text;
    public GameObject torchLight;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (right == null)
        {
            if (trackedObj.index == SteamVR_TrackedObject.EIndex.None)
                return;

            right = SteamVR_Controller.Input((int)trackedObj.index);
        }

        if (countDown > 0f)
        {
            torchLight.GetComponent<Light>().enabled = false;
            countDown -= Time.deltaTime;
            int intCountDown = (int)countDown;
            text.text = intCountDown.ToString();

        }
        if (countDown <= 0f)
        {
            text.text = "Ready";
            torchLight.GetComponent<Light>().enabled = true;
        }

        origin = transform.position;
        direction = transform.forward;
        RaycastHit hit;

        if (right.GetPress(SteamVR_Controller.ButtonMask.Grip))
        {
            Debug.Log("GRIP!");
            if (currentHitObject == null)
            {
                if (countDown > 0f)
                {
                    return;
                }
                if (Physics.SphereCast(origin, sphereRadius, direction, out hit, maxDistance, layerMask, QueryTriggerInteraction.UseGlobal))
                {
                    currentHitObject = hit.transform.gameObject;
                    currentHitDistance = hit.distance;

                    if (currentHitObject.tag == "IA")
                    {
                        currentHitObject.GetComponent<IAInfo>().isGrabbed = true;
                        //currentHitObject.GetComponent<MoveTo>().enabled = false;
                        //currentHitObject.GetComponent<NavMeshAgent>().enabled = false;
                    }
                    else
                        currentHitObject.GetComponent<Rigidbody>().isKinematic = true;
                }


            }
            else
            {
                //TODO: peut-etre remonter cette partie a la fin du IF SphereCast

                currentHitObject.transform.position = Vector3.Lerp(currentHitObject.transform.position, origin, Time.deltaTime);
                countDown = setCountDown;
                //camVr.GetComponent<MoveCamVr>().enabled = false;
                Debug.Log("Origin : " + origin);
                Debug.Log("currentHitObject.transform.position : " + currentHitObject.transform.position);
            }

        }


        if (right.GetPressUp(SteamVR_Controller.ButtonMask.Grip))
        {
            Debug.Log("DEGRIP!");
            if (currentHitObject != null)
            {
                if (currentHitObject.tag != "IA")
                {
                    currentHitObject.GetComponent<Rigidbody>().isKinematic = false;
                }
                else
                {
                    currentHitObject.GetComponent<IAInfo>().isGrabbed = false;
                }
                currentHitObject = null;

                //StartCoroutine("NavMeshAgent");

                //if (currentHitObject.GetComponent<NavMeshAgent>() != null)
                //{

                //}
            }
        }
    }

    IEnumerator NavMeshAgent()
    {
        yield return new WaitForSeconds(wait);
        Debug.Log(currentHitObject);
        if (currentHitObject.tag == "IA")
        {
            currentHitObject.GetComponent<NavMeshAgent>().enabled = true;
            currentHitObject.GetComponent<MoveTo>().enabled = true;
        }

        currentHitObject = null;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(origin, origin + direction * currentHitDistance);
        Gizmos.DrawWireSphere(origin + direction * currentHitDistance, sphereRadius);
    }
}
