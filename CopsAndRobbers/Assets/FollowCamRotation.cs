﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamRotation : MonoBehaviour {
    public GameObject headCam;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(headCam.transform.position.x, headCam.transform.position.y, headCam.transform.position.z -0.3f);

        transform.rotation = headCam.transform.rotation;
    }
}
