﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class SphereCasterTete : MonoBehaviour
{

    

    public GameObject currentHitObject;
    public GameObject Jailtete;
    public GameObject tete;


    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;


    private Vector3 origin;
    private Vector3 originGrab;
    public GameObject originCube;
    public GameObject originGrabCube;
    private Vector3 direction;

    private float currentHitDistance;

    

    public float wait;

    public float setCountDown = 2f;
    public float countDown;

   
    public GameObject torchLight;

    public Animator animator;

    public bool check = false;
    
    public TestMoveTeteIA testMoveTeteIA;
    public bool DepJail = false;
    



    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {


        if (countDown > 0f)
        {

            countDown -= Time.deltaTime;
            int intCountDown = (int)countDown;
            //text.text = intCountDown.ToString();

        }
        //if (countDown <= 0f)
        //{

        //    text.text = "Pret";
        //    torchLight.GetComponent<Light>().enabled = true;
        //}




        origin = transform.position;
        originGrab = new Vector3(transform.position.x, transform.position.y - 1f, transform.position.z);

        originCube.transform.position = origin;
        originGrabCube.transform.position = originGrab;
        //Debug.Log("origin : " + origin+ "      originGrab : " + originGrab);
        direction = transform.forward;
        RaycastHit hit;


        
            if (currentHitObject == null)
            {
                
            if (countDown > 0f)
            {
                return;
            }
                //animator.enabled = true;
            if (Physics.SphereCast(origin, sphereRadius, direction, out hit, maxDistance, layerMask, QueryTriggerInteraction.UseGlobal))
                {
                    currentHitObject = hit.transform.gameObject;
                    currentHitDistance = hit.distance;
                if (currentHitObject.tag == "map" || currentHitObject.tag == "Building")
                {
                    return;
                }

                    
                if (currentHitObject.tag == "IA" || currentHitObject.tag == "Player")
                {

                    //animator.enabled = false;

                    currentHitObject.GetComponent<IAInfo>().isGrabbed = true;
                    currentHitObject.GetComponent<MoveTo>().enabled = false;
                    currentHitObject.GetComponent<NavMeshAgent>().enabled = false;
                }
                //else
                //    currentHitObject.GetComponent<Rigidbody>().isKinematic = true;
                }


            }
            else
            {
            if (currentHitObject.tag != "Building")
            {
                Debug.Log("if builduig");
                currentHitObject.transform.position = Vector3.Lerp(currentHitObject.transform.position, originGrab, Time.deltaTime);
                countDown = setCountDown;
                testMoveTeteIA.relanceDeplaceemnt = false;

            }
                
                //camVr.GetComponent<MoveCamVr>().enabled = false;
                //Debug.Log("Origin : " + origin);
                //Debug.Log("currentHitObject.transform.position : " + currentHitObject.transform.position);
            }
            if (check == true)
            {
                Debug.Log("check true");
            //currentHitObject.transform.position = Vector3.Lerp(currentHitObject.transform.position, originGrab, Time.deltaTime);
            
                Debug.Log("je suis arriver ici");
                currentHitObject.transform.position = originGrab;
                tete.transform.position = Vector3.Lerp(tete.transform.position, Jailtete.transform.position, Time.deltaTime);
            if (DepJail == true)
            {
                check = false;
                Debug.Log("je sui au deesu de la jail");
                if (currentHitObject != null)
                {
                    if (currentHitObject.tag != "IA")
                    {
                        currentHitObject.GetComponent<Rigidbody>().isKinematic = false;

                    }
                    else
                    {
                        currentHitObject.GetComponent<IAInfo>().isGrabbed = false;

                    }
                    currentHitObject = null;
                    testMoveTeteIA.flag = false;
                    testMoveTeteIA.currentIa = null;
                    testMoveTeteIA.relanceDeplaceemnt = true;
                    torchLight.GetComponent<Light>().enabled = true;
                    DepJail = false;
                    //testMoveTeteIA.relanceDeplaceemnt = true;


                    //StartCoroutine("NavMeshAgent");

                    //if (currentHitObject.GetComponent<NavMeshAgent>() != null)
                    //{

                    //}


                }
            }




            //currentHitObject.transform.position = new Vector3 (currentHitObject.transform.position.x, currentHitObject.transform.position.y, currentHitObject.transform.position.z);
        }
        





        //if (currentHitObject != null)
        //{
        //if (currentHitObject.tag != "IA")
        //{
        //    currentHitObject.GetComponent<Rigidbody>().isKinematic = false;
        //}
        //else
        //{
        //    currentHitObject.GetComponent<IAInfo>().isGrabbed = false;

        //}
        //currentHitObject = null;

        //StartCoroutine("NavMeshAgent");

        //if (currentHitObject.GetComponent<NavMeshAgent>() != null)
        //{

        //}


        //}
        //




    }



    //IEnumerator NavMeshAgent()
    //{
    //    yield return new WaitForSeconds(wait);
    //    Debug.Log(currentHitObject);
    //    //if (currentHitObject.tag == "IA")
    //    //{
    //    //    currentHitObject.GetComponent<NavMeshAgent>().enabled = true;
    //    //    currentHitObject.GetComponent<MoveTo>().enabled = true;
    //    //}

    //    currentHitObject = null;
    //}

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(origin, origin + direction * currentHitDistance);
        Gizmos.DrawWireSphere(origin + direction * currentHitDistance, sphereRadius);
    }
}
