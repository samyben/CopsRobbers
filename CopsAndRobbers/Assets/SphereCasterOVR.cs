﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class SphereCasterOVR : MonoBehaviour
{
    public GameObject currentHitObject;
    public float countDown;

    public float setCountDown = 4f;
    public float distanceToAttract = 0.3f;
    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;
    public LayerMask layerMaskObstacle;
    public Text text;
    public GameObject torchLight;
    public Transform rightHandAvatar;

    private Vector3 origin;
    private Vector3 direction;
    private float currentHitDistance;
    private float playerInfoGold;
    private bool playerIsGrabbed = false;
    private float distanceRaycast = Mathf.Infinity;
    public Color lightColorBase;
    public Color lightColorGrip;
    public float fixHeight;


    // Update is called once per frame
    void Update()
    {
        if (countDown > 0f)
        {
            torchLight.GetComponent<Light>().enabled = false;
            countDown -= Time.deltaTime;
            int intCountDown = (int)countDown;
            text.text = intCountDown.ToString();

        }
        if (countDown <= 0f)
        {
            text.text = "Ready";
            torchLight.GetComponent<Light>().enabled = true;
        }

        origin = transform.position;
        direction = transform.forward;
        RaycastHit hit;

        if (OVRInput.Get(OVRInput.Button.SecondaryHandTrigger))
        {

            torchLight.GetComponent<Light>().color = lightColorGrip;

            if (currentHitObject == null)
            {
                distanceRaycast = Mathf.Infinity;
            }
            else
            {
                distanceRaycast = Vector3.Distance(currentHitObject.transform.position, origin);
            }

            if (!Physics.Raycast(origin, direction, out hit, distanceRaycast, layerMaskObstacle))
            {
                Debug.DrawRay(transform.position, direction * hit.distance, Color.yellow);
                if (currentHitObject == null)
                {
                    if (countDown > 0f)
                    {
                        return;
                    }

                    if (Physics.SphereCast(origin, sphereRadius, direction, out hit, maxDistance, layerMask, QueryTriggerInteraction.UseGlobal))
                    {
                        currentHitObject = hit.transform.gameObject;
                        currentHitDistance = hit.distance;

                        if (currentHitObject.tag == "IA")
                            currentHitObject.GetComponent<IAInfo>().isGrabbed = true;
                        else
                            currentHitObject.GetComponent<Rigidbody>().isKinematic = true;
                    }

                }
                else
                {
                    //TODO: peut-etre remonter cette partie a la fin du "if Physics.SphereCast()"

                    currentHitObject.transform.position = Vector3.Lerp(currentHitObject.transform.position, origin, Time.deltaTime);

                    if (Vector3.Distance(currentHitObject.transform.position, origin) <= distanceToAttract)
                    {
                        Debug.Log("Player Grabbed");
                        currentHitObject.transform.parent = this.transform;
                        playerIsGrabbed = true;
                        torchLight.transform.parent.gameObject.SetActive(false);


                        countDown = setCountDown;
                    }

                    if (playerIsGrabbed)
                    {
                        if (currentHitObject.tag == "Player")
                        {
                            playerInfoGold = currentHitObject.GetComponent<PlayerInfo>().goldOnPlayer;
                            Debug.Log("Call Grabbed");
                            PlayerManager.instance.Grabbed(currentHitObject);
                        }
                        currentHitObject.transform.position = new Vector3(rightHandAvatar.position.x, rightHandAvatar.position.y - fixHeight, rightHandAvatar.position.z);
                        currentHitObject.transform.rotation = new Quaternion(rightHandAvatar.rotation.x, rightHandAvatar.rotation.y, rightHandAvatar.rotation.z, rightHandAvatar.rotation.w);
                    }
                    //camVr.GetComponent<MoveCamVr>().enabled = false;
                }
            }
            else
            {
                Debug.DrawRay(transform.position, direction * hit.distance, Color.yellow);
            }

        }


        if (OVRInput.GetUp(OVRInput.Button.SecondaryHandTrigger))
        {
            torchLight.GetComponent<Light>().color = lightColorBase;
            Debug.Log("DEGRIP!");

            if (currentHitObject != null)
            {
                currentHitObject.transform.SetParent(null);
                playerIsGrabbed = false;
                torchLight.transform.parent.gameObject.SetActive(true);

                if (currentHitObject.tag != "IA")
                {
                    currentHitObject.GetComponent<Rigidbody>().isKinematic = false;
                    currentHitObject.GetComponent<PlayerInfo>().goldCoin.Stop();
                }
                else
                {
                    currentHitObject.GetComponent<IAInfo>().isGrabbed = false;
                }
                currentHitObject = null;
            }
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(origin, origin + direction * currentHitDistance);
        Gizmos.DrawWireSphere(origin + direction * currentHitDistance, sphereRadius);
    }
}
